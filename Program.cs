﻿using System;

namespace matriceTab
{
    class Program
    {
        static void Main(string[] args)
        {
            Matrix ma1 = new Matrix(30,0);
            Console.WriteLine(ma1.ToString());

            ma1.Set(2,1,3);
            Console.WriteLine(ma1.ToString());

            Console.WriteLine(ma1.GetSize(2,1));
        }
    }
}
