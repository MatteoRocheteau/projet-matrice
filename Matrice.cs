using System;

namespace matriceTab
{
    class Matrix {
        
        private int [,] matrix;
        private int size;

        public Matrix (int size, int defaultValue) { //Nom de la fonction : Matrice, créé une Matrice.
            this.matrix = new int [size, size];
            this.size = size;
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++){
                    this.matrix[i, j] = defaultValue;
                }
            }
        }

        public int GetSize (int x, int y) { //Nom de la fonction : Getter , récupère la valeur voulue de la matrice (De l'emplacement souhaité)
            //return this.size;
            return this.matrix[x,y];
        }

        public void Set (int x, int y, int val) { ////Nom de la fonction : Setter , Affecte la valeur voulue à la matrice (à l'emplacement souhaité)
            this.matrix[x,y] = val;
        }

        public string ToString() { //Nom de la fonction : Print Matrice, Sert à mettre en forme la matrice
            string ret = "";
            for (int i = 0; i < this.size; i++) {
                ret = ret + " ";
            for (int j = 0; j < this.size; j++) {
                ret = ret + this.matrix[i,j] + " ";
            }
            ret = ret + "\n";
            }
            return ret;
        }
    }
}